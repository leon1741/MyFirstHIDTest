# MyFirstHIDTest

#### 介绍
WIN10+VS2017环境下，使用相关库函数，实现一个最简单的HID设备枚举及其具体信息查询的程序，用来了解HID设备的驱动及通信流程。

开发环境的搭建过程，详见我的CSDN博客【[如何在win10+VS2017环境下安装USB驱动开发套件WDK](https://blog.csdn.net/LEON1741/article/details/87258599)】

下面是在我本人的电脑上运行之后输出的信息：

```c
Begin to list all HID device...

Get HID Guid: Data1[0x4D1E55B2]. Data2[0xF16F]. Data3[0x11CF]. Data4[0x88CB011110030]

try deviceNo 0.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_413c&pid_2113&mi_01&col01#8&d329eae&0&0000#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[264]. ProductID[0x2113]. VendorID[0x413C]

try deviceNo 1.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_09da&pid_054f&mi_00&col01#7&20cf2917&0&0000#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[628]. ProductID[0x54F]. VendorID[0x9DA]

try deviceNo 2.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_09da&pid_054f&mi_00&col02#7&20cf2917&0&0001#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[628]. ProductID[0x54F]. VendorID[0x9DA]

try deviceNo 3.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_09da&pid_054f&mi_00&col03#7&20cf2917&0&0002#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[628]. ProductID[0x54F]. VendorID[0x9DA]

try deviceNo 4.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_09da&pid_054f&mi_00&col04#7&20cf2917&0&0003#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[628]. ProductID[0x54F]. VendorID[0x9DA]

try deviceNo 5.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_413c&pid_2113&mi_01&col02#8&d329eae&0&0001#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[264]. ProductID[0x2113]. VendorID[0x413C]

try deviceNo 6.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_09da&pid_054f&mi_01#7&30814ab&0&0000#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[628]. ProductID[0x54F]. VendorID[0x9DA]

try deviceNo 7.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_413c&pid_301a#7&5745a0d&0&0000#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[256]. ProductID[0x301A]. VendorID[0x413C]

try deviceNo 8.
----Get devInfoData: cbSize[32]. Flags[0x01]. InterfaceClassGuid([0x4D1E55B2] [0xF16F] [0x11CF] [0x88CB011110030])
----Get devDetail: cbSize[8] DevicePath[\\?\hid#vid_413c&pid_2113&mi_00#8&359d4c77&0&0000#{4d1e55b2-f16f-11cf-88cb-001111000030}]
----Get hidAttributes: Size[12]. VersionNumber[264]. ProductID[0x2113]. VendorID[0x413C]

try deviceNo 9.
No More Item Left!!!

Search is Over!!! find 9 HID_Dev altogether in your computer...

任务完成，按任意键退出...

```

