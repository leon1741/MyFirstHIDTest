#include <windows.h>
#include <stdio.h>
#include <winnt.h>
#include <errno.h>
#include <time.h>
#include <string.h>

#include <hidsdi.h>
#include <setupapi.h>

/*********************************************************************************************************************/
//                           模块宏定义
/*********************************************************************************************************************/
#define OVERLAP_MODE         0                                                 /* 是否要启用重叠异步模式打开HID设备文件 */

/*********************************************************************************************************************/
// 函数功能：主函数 - 用来列举出系统中所有可用的HID设备信息，并尝试打开他们获取其属性
// 输入参数：
// 输出参数：
// 返回参数：
/*********************************************************************************************************************/
int main(int argc, char** argv)
{
    int                       deviceNo;
    bool                      result;
    HANDLE                    hidHandle;
    GUID                      hidGuid;
    ULONG                     requiredLength;
    SP_DEVICE_INTERFACE_DATA  devInfoData;

    deviceNo = 0;
    hidHandle = NULL;
    devInfoData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

    printf("Begin to list all HID device...\r\n\r\n");

    /* HidD_GetHidGuid routine returns the device interfaceGUID for HIDClass devices - from MS */
    HidD_GetHidGuid(&hidGuid);

    printf("Get HID Guid: Data1[0x%X]. Data2[0x%X]. Data3[0x%X]. Data4[0x%X%X%X%X%X%X%X%X]\r\n", hidGuid.Data1, hidGuid.Data2, hidGuid.Data3,
        hidGuid.Data4[0], hidGuid.Data4[1], hidGuid.Data4[2], hidGuid.Data4[3], hidGuid.Data4[4], hidGuid.Data4[5], hidGuid.Data4[6], hidGuid.Data4[7]);

    /* SetupDiGetClassDevs返回一个包含本机上所有被请求的设备信息的设备信息集句柄 */
    /* SetupDiGetClassDevs function returns a handle to a device information set that contains requested device information elements for a local computer - from MS */
    HDEVINFO hDevInfo = SetupDiGetClassDevs(&hidGuid, NULL, NULL, (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

    if (hDevInfo == INVALID_HANDLE_VALUE) {
        printf("Fatal Error: SetupDiGetClassDevs Fail!!!\r\n");
        return 1;
    }

    SetLastError(NO_ERROR);                                                    /* 首先清空错误代码，以便于后面的使用 */

    while (1) {

        printf("\r\ntry deviceNo %d.\r\n", deviceNo);

        /* The SetupDiEnumDeviceInterfaces function enumerates the device interfaces that are contained in a device information set - from MS */
        result = SetupDiEnumInterfaceDevice(hDevInfo, 0, &hidGuid, deviceNo, &devInfoData);

        if ((result == false) || (GetLastError() == ERROR_NO_MORE_ITEMS)) {    /* 出现ERROR_NO_MORE_ITEMS错误表示已经找完了所有的设备 */
            printf("No More Item Left!!!\r\n\r\n");
            break;
        } else {
            printf("----Get devInfoData: cbSize[%.2d]. Flags[0x%.2X]. InterfaceClassGuid([0x%X] [0x%X] [0x%X] [0x%X%X%X%X%X%X%X%X])\r\n",
                devInfoData.cbSize, devInfoData.Flags, devInfoData.InterfaceClassGuid.Data1, devInfoData.InterfaceClassGuid.Data2, devInfoData.InterfaceClassGuid.Data3,
                devInfoData.InterfaceClassGuid.Data4[0], devInfoData.InterfaceClassGuid.Data4[1], devInfoData.InterfaceClassGuid.Data4[2], devInfoData.InterfaceClassGuid.Data4[3],
                devInfoData.InterfaceClassGuid.Data4[4], devInfoData.InterfaceClassGuid.Data4[5], devInfoData.InterfaceClassGuid.Data4[6], devInfoData.InterfaceClassGuid.Data4[7]);
        }

        /* The SetupDiGetDeviceInterfaceDetail function returns details about a device interface - From MS */

        requiredLength = 0;                                                                                       /* 先将变量置零，以便于下一步进行获取 */
        SetupDiGetInterfaceDeviceDetail(hDevInfo, &devInfoData, NULL, 0, &requiredLength, NULL);                  /* 第一次调用，为了获取requiredLength */
        PSP_INTERFACE_DEVICE_DETAIL_DATA devDetail = (SP_INTERFACE_DEVICE_DETAIL_DATA *)malloc(requiredLength);   /* 根据获取到的长度申请动态内存 */
        devDetail->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);                                              /* 先对变量进行部分初始化 */
        result = SetupDiGetInterfaceDeviceDetail(hDevInfo, &devInfoData, devDetail, requiredLength, NULL, NULL);  /* 第二次调用，为了获取devDetail */

        if (result == false) {
            printf("Fatal Error: SetupDiGetInterfaceDeviceDetail fail!!!\r\n");
            free(devDetail);
            SetupDiDestroyDeviceInfoList(hDevInfo);
            return 1;
        } else {
            printf("----Get devDetail: cbSize[%.2d] DevicePath[%s]\r\n", devDetail->cbSize, devDetail->DevicePath);
        }

        if (OVERLAP_MODE == 1) {
            hidHandle = CreateFile(devDetail->DevicePath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        } else {
            hidHandle = CreateFile(devDetail->DevicePath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
        }

        free(devDetail);

        if (hidHandle == INVALID_HANDLE_VALUE) {                               /* 系统会将部分HID设备设置成独占模式 */
            printf("CreateFile fail!!! dev maybe in_use. continue to try next one...\r\n");
            ++deviceNo;
            continue;
        }

        _HIDD_ATTRIBUTES hidAttributes;

        result = HidD_GetAttributes(hidHandle, &hidAttributes);                /* 获取HID设备的属性 */

        if (result == false) {
            printf("Fatal Error: HidD_GetAttributes fail!!!\r\n");
            CloseHandle(hidHandle);
            SetupDiDestroyDeviceInfoList(hDevInfo);
            return 1;
        } else {
            printf("----Get hidAttributes: Size[%.2d]. VersionNumber[%d]. ProductID[0x%.4X]. VendorID[0x%.4X]\r\n",
                hidAttributes.Size, hidAttributes.VersionNumber, hidAttributes.ProductID, hidAttributes.VendorID);
        }

        CloseHandle(hidHandle);
        ++deviceNo;
    }

    /* The SetupDiDestroyDeviceInfoList function deletes a device information set and frees all associated memory - From MS */
    SetupDiDestroyDeviceInfoList(hDevInfo);
    printf("Search is Over!!! find %d HID_Dev altogether in your computer...\r\n\r\n", deviceNo);

    printf("任务完成，按任意键退出...\r\n");
    getchar();
    getchar();

    return 0;
}

/* CreateFile函数使用说明：

CreateFile(devDetail->DevicePath,                //设备路径
           GENERIC_READ | GENERIC_WRITE,         //访问方式
           FILE_SHARE_READ | FILE_SHARE_WRITE,   //共享模式
           NULL,
           OPEN_EXISTING,                        //文件不存在时，返回失败
           FILE_FLAG_OVERLAPPED,                 //以重叠（异步）模式打开
           NULL);

1、 访问方式：
    假如是系统占设备，例如鼠标、键盘等等，应将此参数设置为0，否则后续函数操纵将失败（譬如HidD_GetAttributes）；
    也就是说，不能对独占设备进行除了查询以外的任何操纵，所以能够使用的函数也是很有限的，下文的一些函数并不一定适合这些设备。
    在此顺便列出MSDN上关于此参数的说明：
    If this parameter is zero, the application can query file and device attributes without accessing the device.
    This is useful if an application wants to determine the size of a floppy disk drive and the formats it supports without requiring a floppy in the drive.
    It can also be used to test for the file’s or directory’s existence without opening it for read or write access

2、 重叠（异步）模式：
    此参数并不会在此处表现出明显的意义，它主要是对后续的WriteFile，ReadFile有影响。
    假如这里设置为重叠（异步）模式，那么在使用WriteFile，ReadFile时也应该使用重叠（异步）模式，反之亦然。
    这首先要求WriteFile，ReadFile的最后一个参数不能为空（NULL）。否则，便会返回87（参数错误）错误号。
    当然，87号错误并不代表就是此参数不正确，更多的信息将在具体讲述这两个函数时指出。
    此参数为0时，代表同步模式，即WriteFile，ReadFile操纵会在数据处理完成之后才返回，否则阻塞在函数内部。
*/
